const nodemailer = require('nodemailer')
const moment = require('moment')
const ical = require('ical-generator');

var smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: "kyserwor@gmail.com",
        pass: "ftwlkekcxoijuvrs"
    }
});

// var mailOptions = {
//     from: "kyserwor@gmail.com",
//     to: "rlukas@frobese.com, kyserwor@gmail.com",
//     subject: "This is a test email from a developer",
//     html: "<h1>Welcome to my website</h1>"
// }


async function sendemail(sendto, subject, htmlbody, calendarObj = null) {
    mailOptions = {
        from: "kyserwor@gmail.com",
        to: `${sendto}, "kyserwor@gmail.com`, 
        subject: subject,
        html: htmlbody
    }
    if (calendarObj) {
        console.log("hier !!!!!!!!")
        let alternatives = {
            "Content-Type": "text/calendar",
            "method": "REQUEST",
            "content": Buffer.from(calendarObj.toString()),
            "component": "VEVENT",
            "Content-Class": "urn:content-classes:calendarmessage"
        }
        mailOptions['alternatives'] = alternatives;
        mailOptions['alternatives']['contentType'] = 'text/calendar'
        mailOptions['alternatives']['content']
            = Buffer.from(calendarObj.toString())
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: ", response);
        }
    })
}

function getIcalObjectInstance(starttime, endtime, summary, description, location, url, name, email) {
    const cal = ical({ domain: require('os').hostname(), name: 'My test calendar event' });
    cal.domain(require('os').hostname());
    cal.createEvent({
        start: starttime,         // eg : moment()
        end: endtime,             // eg : moment(1,'days')
        summary: summary,         // 'Summary of your event'
        description: description, // 'More description'
        location: location,       // 'Delhi'
        url: url,                 // 'event url'
        organizer: {              // 'organizer details'
            name: name,
            email: email
        },
        method: 'request',
    });
    return cal;
}

let icalVar = getIcalObjectInstance(moment(), moment(1, 'days'), "summary", "description", "location", "url", "testName", "email");

sendemail("ramon.lukas4@gmail.com", "subject", "<h1>Test</h1>", icalVar);
