var nodemailer = require("nodemailer");
const ical = require('ical-generator');

var nodemailer = require("nodemailer");
var smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: "tesgmailaccount@gmail.com",
        pass: "gmailapppassword"
    }
});
async function sendemail(sendto, subject, htmlbody, calendarObj = null) {
    mailOptions = {
        to: sendto,
        subject: subject,
        html: htmlbody
    }
if (calendarObj) {
        let alternatives = {
            "Content-Type": "text/calendar",
            "method": "REQUEST",
            "content": new Buffer(calendarObj.toString()),
            "component": "VEVENT",
            "Content-Class": "urn:content-classes:calendarmessage"
        }
mailOptions['alternatives'] = alternatives;
mailOptions['alternatives']['contentType'] = 'text/calendar'
mailOptions['alternatives']['content'] 
    = new Buffer(calendarObj.toString())
}
smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " , response);
        }
    })
}


function getIcalObjectInstance() {
    const cal = ical({ domain: "mytestwebsite.com", name: 'My test calendar event' });
    cal.domain("mytestwebsite.com");
    cal.createEvent({
        start: moment(),         // eg : moment()
        end: moment(1,'days'),             // eg : moment(1,'days')
        summary: 'Summary of your event',         // 'Summary of your event'
        description: 'More description', // 'More description'
        location: 'Delhi',       // 'Delhi'
        url: 'event url',                 // 'event url'
        organizer: {              // 'organizer details'
            name: 'organizer details name',
            email: 'organizer details email'
        },
    });
    return cal;
}

module.exports = {
    sendemail,
};