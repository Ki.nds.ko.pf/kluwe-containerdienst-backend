var icalToolkit = require("ical-toolkit");
var postmark = require("postmark");
var client = new postmark.Client("4f61d6f3-9715-46b5-b812-0eb09300d756");
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");

//Create a iCal object
var builder = icalToolkit.createIcsFileBuilder();

builder.events.push({
  start: new Date(2021, 02, 19, 10, 30),
  end: new Date(2021, 02, 19, 12, 30),
  timestamp: new Date(),
  summary: "Container Termin Test",
  location: "Home",
  description: "Testing it!",
  adress: "Grahles Garten 6, 30900 Wedemark",
  categories: [{ name: "MEETING" }],
  attendees: [
    {
      rsvp: true,
      name: "Gmail Rlukas",
      email: "ramon.lukas3@gmail.com",
    },
    {
      rsvp: true,
      name: "Outlook Felix",
      email: "fkluwe@kluwe-containerdienst.de",
    },
  ],
  organizer: {
    name: "Felix Orga",
    email: "fkluwe@kluwe-containerdienst.de",
  },
});
//Add the event data

var icsFileContent = builder.toString();
var smtpOptions = {
  host: "smtp.postmarkapp.com",
  port: 2525,
  secureConnection: true,
  auth: {
    user: "4f61d6f3-9715-46b5-b812-0eb09300d756",
    pass: "4f61d6f3-9715-46b5-b812-0eb09300d756",
  },
};

var transporter = nodemailer.createTransport(smtpTransport(smtpOptions));

var mailOptions = {
  from: "sender@example.com",
  to: builder.events[0].attendees[0].email,
  subject: "Meeting to attend",
  html: "Anything here",
  text: "Anything here",
  alternatives: [
    {
      contentType: 'text/calendar; charset="utf-8"; method=REQUEST',
      content: icsFileContent.toString(),
    },
  ],
};

//send mail with defined transport object
transporter.sendMail(mailOptions, function (error, info) {
  if (error) {
    console.log(error);
  } else {
    console.log("Message sent: " + info.response);
  }
});
